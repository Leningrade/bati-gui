import './App.css';
import {
  Switch,
  Route
} from 'react-router-dom'
import { Component } from 'react'
import {populateStore} from './store/index.js'

import Menu from './components/Menu.js'

import Home from './views/Home.js'
import Detail from './views/Detail.js'
import About from  './views/About.js'
import Error from './views/Error.js'


class App extends Component {
  async componentDidMount() {
    // populate store on initial load
    await populateStore();
    // verify global state
    console.log('Store Populated')
  }
  render() {
    return (
      <main>
        <Menu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/detail/:id" component={Detail} />
          <Route exact path="/about" component={About} />
          <Route component={Error}/>
        </Switch>
      </main>
    );
  }
}

export default App;
