## Remotes

En este folder se documentan los contratos y llamados para consultas externas  a la plataforma

### atención
Para el uso de la API de HeroDatabase ese necesario tramitar un API Key único. Para usar este proyecto, asegurate que esta clave este configurada en la variable de ambiente *REACT_APP_HERO_API_KEY*.

~para fines de esta demo, la KEY se consumira de manera local~

### Sobre la estructura de las llamdas

Esta prueba funciona bajo la siguiente arquitectura de llamadas. Para fines de la prueba, uno de estos endpoints es simulado con un JSON.

diagrama de llamadas: 


# Remotos
- `/api`: Se consume en Index.js para enlistar los superheroes a mostrar. Url Base
- `/api/config`: lista de personajes y estado de acces. Configura la UI e identifica cuáles personajes obtener, cuales personajes  mantener bloqueados. En un escenario real esto debería de estar ligado a la identidad de un usuario. Para fines de la prueba se consumirá el ejemplo de contrato de JSON.
- `/api/:id` :  se consume para obtener los detalles del personaje. 