// basado en el contrato para la lista de caracteres
// genera una configuración random de la interfáce
import {removeDuplicate} from '../../assets/helpers.js'


let randomCharacterList = [],
    minCharacters = 1,
    maxRequested = 40,
    maxCharacters = 768;
    
function imageSelect(value) {
    // value between 0 and 3
    if (value <= 7) {
        return 'blocked'
    } else if (value <= 9) {
        return 'new'
    }
    return 'seen'
}

for (let i = minCharacters; i <= maxRequested; i++) {
    const id = Math.floor(Math.random() * maxCharacters)
    const imgBase = Math.random() * 10
    const object = {
        id: id.toString(),
        status: imageSelect(imgBase)
    }
    const findRepeat = randomCharacterList.includes(object)
    if (!findRepeat) {
        randomCharacterList.push(object)
        
    }
}
export const cleanCharacterList = removeDuplicate(randomCharacterList)

export default cleanCharacterList
