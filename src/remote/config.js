const heroKey = process.env.REACT_APP_HERO_API_KEY
export const heroURL = 'http://superheroapi.com/api/'
export const baseURL = `${heroURL}${heroKey}/`
export const proxyURL = 'http://localhost:3001/API'

let configs = {
    heroURL,
    baseURL,
    proxyURL
}

export default configs