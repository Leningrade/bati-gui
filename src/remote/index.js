import axios from 'axios'
import config from './config.js'
import randomCharacterList from './examples/randomCharacterList.js'

const http = axios.create({
    baseURL: config.proxyURL,
    timeout: 10000,
})

export function getCharacterList() {
  return randomCharacterList
}

export async function getCharacterInfo(id) {
    const response = await http.get(`/${id}`)
    return response.data
}

export function getCharacterImage(id) {
    //regresa la promesa para ser usada en un array de promesas
    return http.get(`/${id}/image`)
}

export async function getAll(array) {
    /* 
        recive el array de objetos personajes, y manda a llamar todos en paralelo
        Eg: 
        [
            { id, name, status}
        ]
    */ 
    
    // 1. construye un array de promesas para poder llamarlas en threads
    const promises = array.map((item) => {
        return http.get(`/${item.id}`)
    })

    // 2. llama los objetos en paralelo
    // usamos Promise por un error desconocido de Axios
    // console.log('solving flag 2')
    const response = await Promise.all(promises)

    // 3.  cleanup data structure
    // console.log('solving flag 3')
    const cleanData = response.map((datum) => {
        const object = datum.data
        // agrega el estatus al objeto encontrado
        const findId = array.find(element => element.id === object.id )
        if(findId) {
            object.status = findId.status
        }
        return object;
    })
    // remove errros from response
    console.log('not clean', cleanData)

    const filteredData = cleanData.filter((entry) => {
        return entry.response === "success"
    })
    console.log('clean', filteredData)

    // console.log('solving flag 4')
    // 4. envia de regreso las promesas resueltas
    return filteredData
}

let collection = {
    getCharacterInfo,
    getCharacterList
}

export default collection