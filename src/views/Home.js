import { Component } from 'react'
import CharacterSelect from '../components/characterSelect.js';
import { isEmpty } from '../assets/helpers.js'
import characterStore from '../store/index.js'


import './Home.css'
import newspaperPNG from '../assets/newspaper.png'

class Home extends Component {
    render () {
        const conditional  = !isEmpty(this.props.characterList)
        if(conditional) {
            const { characterList } = this.props 
            // create array of images for character select
            let characterSelectArray = characterList.map((datum, index) => {
                if (datum !== null) {
                    // console.log(datum)
                    const props = {
                        url: datum.image.url,
                        alt: datum.name,
                        id: datum.id,
                        status: datum.status,
                        name: datum.name
                    }
                    return <CharacterSelect
                        object={props}
                        url={props.url} 
                        name={props.name}
                        key={props.id + props.status + index}  
                        id={props.id} 
                        alt={props.alt}
                        status={props.status}
                        />  
                }
                
            })

            // return render
            return (
            
                <article className="home home-background">
                    <div className="border-bottom mb-2  ">
                        <h1 className="x4  has-text-white has-background-danger is-single=line"> Explore Today's Biggest Heroes</h1>
                    </div>

                    <div className="columns is-variable is-1-tablet">
                    <Cover url={this.props.currentCover.url}  name={this.props.currentCover.name}   id={this.props.currentCover.id}/>

                    <section className="charachterSelect column">
                        <div className="columns is-multiline is-mobile is-variable is-1-mobile is-1-tablet">
                            {characterSelectArray}
                        </div>
                    </section>
                    
                    </div>
                </article>
            )
        } else { 
            return Loader()
        }
       
    }
}
function Cover({url, name, id}) {
    // lateral derecha que muestra las imágenes;

    if (url && name) {
        return (
            <section className="image-cover column is-relative is-4-desktop is-hidden-mobile">
                <figure className="image is-sticky">
                    <img src={url} alt="" />
                    <CoverName name={name} id={id}/>
                </figure>
                
            </section>
        )
    } else {
        return (
            <section className="is-grid is-relative column is-fullscreen is-4-desktop is-hidden-mobile">
                <div className=" is-center is-sticky"> 
                    <span className="exclamation has-shadow font-black "> !</span>  
                    <span className="exclamation-sidekick x1 is-uppercase">select a hero</span>
                </div>
            </section>
        )
    }

    
}

function CoverName({name}) {
    return (
        <div className="is-absolute is-top ">
            <h1 className="x6 has-shadow"> {name} </h1>
        </div>
        
    )
}
function Loader() {
    // spinner que se muestra al cargar la presentación
    return (
        <section className="is-grid">
            <figure className="newspaper-spin"> <img src={newspaperPNG} alt="newspaper" /></figure>
        </section>
    )
}


function mapStateToProps(state, ownProps) {
    return {
        characterList: state.characterList,
        currentCover: state.hoveredImage
    }

}

export default characterStore.connect(mapStateToProps)(Home);