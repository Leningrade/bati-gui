import { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import Datum from '../components/detailDatum.js'
import { isEmpty } from '../assets/helpers.js'
import { characterStore, findCharacter } from '../store/index.js'

import './Detail.css'

class Detail extends Component {
    constructor(props){
        super(props)
        this.state = {
            id: this.props.match.params.id,
            loaded: false,
            onUpdate: null,
        }
    }
    async componentDidMount() {
        this.getCharacterData()
        window.scrollTo(0, 0)

    }
  
    getCharacterData() {
        const data = findCharacter(this.state.id)
        if (data.status === "loading") {
            // timer que se ejecuta ne caso que se intente recargar la página desde la sección de detalle.
            setTimeout(() => {  this.getCharacterData() }, 500);
        }
        console.log('got character', data)
    }
    render () {
        // object destructuring, para facilitar la asignación
        // y hacer más claro desde el principio qué si se va a estar usando
        // guardas de ruta
        if (this.props.loaded && !isEmpty(this.props.content)) {
            switch (this.props.content.status) {
                case 'not-available':
                    return <h1> Resource not available for your account </h1>
                case 'not-allowed':
                    return <h1> Resource not allowed </h1>
                case 'seen':
                    return this.templateDefault(this.props.content)
                default:
                    return <h1>Default</h1>
            }
        }
        return <h1> Loading </h1>

    }
    templateDefault(data) {
        let {
                biography: bio, 
                name, 
                powerstats: power, 
                image: img,
                work
            } = data
            
        return (
            <article className="detail">
                {/* Encabezado, Nombre en grande */}
                <section className="header is-clipped border-bottom ">
                    <h1 className="detail-header  has-shadow ">{name}</h1>
                </section>
                <section className="is-clipped border-bottom has-background-danger ">
                    <Link to="/">
                    
                    <h2 className="has-text-white">Return</h2>
                    </Link>
                </section>
                
                <div className="columns is-gapless border-bottom mb-0">
                    {/* Imágen principal del personaje */}
                    <div className="column is-one-quarter">
                        <section className="headImage" id="main-image">
                            <figure className="image is-3by4"><img src={img ? img.url : ''} alt={`${name}`} /></figure>
                        </section>
                    </div>

                    <div className="column " id="info">
                        {/* Detalles del personaje */}
                        <section className="details">
                            <section className="bio border-bottom ">
                                <div className="container px-3 py-4">
                                    <div className="columns ">
                                        <Datum name="Real Name" column="large" value={bio['full-name']} />
                                    </div>
                                </div>
                        
                            </section>
                            <section className="more-info border-bottom">
                                    <div className="container px-3 py-4">
                                        <div className="columns is-multiline">
                                            <Datum name="First Appearance"value={bio['first-appearance']} />
                                            <Datum name="Publisher"value={bio['publisher']} />
                                        </div>
                                    </div>

                            </section>
                            <section className="more-info border-bottom">
                                    <div className="container px-3 py-4">
                                        <div className="columns is-multiline">
                                            <Datum name="Occupation"value={work['occupation']} />
                                            <Datum name="Base"value={work['Base']} />
                                        </div>
                                    </div>

                            </section>
                            <section className=" power">
                                <div className="container px-3 py-4">
                                    <div className="columns is-multiline is-mobile">
                                        <Datum name="intelligence" className="" value={power.intelligence}/> 
                                        <Datum name="strength" value={power.strength}/> 
                                        <Datum name="speed" value={power.speed}/> 
                                        <Datum name="durability" value={power.durability}/> 
                                        <Datum name="power" value={power.power}/> 
                                        <Datum name="combat" value={power.combat}/> 
                                    </div>
                                </div>
                            </section>
                           
                        </section>
                    </div>
                </div>
                <section className="is-clipped border-bottom has-background-danger ">
                    <Link to="/">
                    
                    <h2 className="has-text-white">Return</h2>
                    </Link>
                </section>          
            </article> 
            )
    }

}



function mapStateToProps(state, ownProps) {
    return {
        content: state.activeDetail,
        loaded: state.loaded,
        characterList: state.characterList
    }
}
// es necesario usar "withRoouter" para
// tener acceso nativo a los parametros de la URI
export default characterStore.connect(mapStateToProps)(withRouter(Detail));