function ErrorPage() {
    return ( 
        <section className="container center">
            There was an error rendering your page
        </section>
    )
}

export default ErrorPage