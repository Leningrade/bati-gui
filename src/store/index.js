import {getAll, getCharacterList} from '../remote/index.js'

import { Store, connectDevTools } from 'simple-react-store'

export const characterStore = new Store({
    characterList: {},
    loaded: false,
    activeDetail: {},
    hoveredImage: {},
});

connectDevTools(characterStore);

export const populateStore = async () => {
    const list = getCharacterList()
    const response = await getAll(list)
    // console.log('loaded store data')
    characterStore.updateState((updatedState) => {
        updatedState.characterList = response
        updatedState.loaded = true
    },
        'Create initial state')
    //console.log(characterStore.getState())
}

export async function awaitStore() {
    let store = characterStore.getState()
    while (!store.loaded) {
        
    }
    return

}

export const findCharacter = (id) => {
    const state = characterStore.getState()
    let status
    // primero intenta encontrar el objeto en el estado

    // only check if it is still loading
    if (state.loaded) {
        const character = state.characterList.find((entry) => entry.id === id)

        if (character === undefined) {
            status = { status: 'not-available' }

            characterStore.updateState((upcoming) => {
                upcoming.activeDetail = status
            }, `${id} not available`)
            
            return status

        }
        // handle blocked requests
        if (character.status === 'blocked') {
            status = { status: 'not-allowed' }
            characterStore.updateState((upcoming) => {
                upcoming.activeDetail = status
            }, `${id} not allowed`)
            return status
        }
        // if it passes all cases
        characterStore.updateState((upcoming) => {
            upcoming.activeDetail = character
        }, `found ${id}`)
        return character

        
    }

    // if loading is false, tell the front what to do
    status = { status: 'loading'}
    return status
}


export const updateCharacterStatus = (id, status) => {
    const state = characterStore.getState()
    const index = state.characterList.findIndex(e => e.id === id)

    // make a shallow clone
    const newState = [...state.characterList ]
    newState[index].status = status
    characterStore.updateState((upcomingState) => {
        upcomingState.characterList = newState
    }, `updated item ${id} to ${status}`)

}

export const chooseHoverImage = (character) => {
    const newCharacter = {...character}
    console.log('upcomming Character', newCharacter)
    characterStore.updateState((upcomming) => {
        upcomming.hoveredImage = newCharacter
    }, `set image to ${character.url}`)
}
// export const appContainer = characterStore.connect(resolveProps)(App)

export default characterStore;