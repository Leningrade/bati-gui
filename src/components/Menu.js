import {Link, useRouteMatch} from 'react-router-dom'
import './menu.css'
// Crear lista de menu anticipadamente. sirve por si acaso después se quiere cambiar la estructura,
//  o para poder consumir la configuración desde algun servicio
let home =     {
        url: '/',
        name: 'Home',
        content: 'ACTION'
    }

let menuItems = [
    {
        url:'/about',
        name: 'About',
        content: 'About'
    }
]

// Componente  de menu
function MenuItem({url, content, logo}) {

    // Regresa el nombre de clase 
    // si el elemento está seleccionado
    let route = {
        path: url,
        exact: true,
    }
    let matchRoute = useRouteMatch(route) ? 'is-current' : ''

    return( 
        <Link 
            to={url} 
            className = {`
                ${logo? 'is-logo': ''}
                navbar-item
                x3 
                is-italic 
                font-black 
                is-uppercase
                ${matchRoute}
                `}>
            {content}
        </Link>
    )
}


// render list to keep jsx cleaner
let menuList = menuItems.map((item, index) => {
    return <MenuItem url={item.url} content={item.content} key={index} />
})



function MainMenu() {
    return (
        <nav className="navbar border-bottom menu-background" role="navigation" aria-label="Main Menu">
            <div className="navbar-brand">
                <MenuItem logo={true} url={home.url} content={home.content} />
            </div>
            <div className="navbar-menu ">
                {menuList}
            </div>
            <ul>
                
            </ul>
        </nav>
    );
}

export default MainMenu