import './detailHeader.css';


function DetailHeader({name, company}) {
    return <h1 className="x9 is-uppercase border-bottom has-line-height-1 ">{name}</h1>
}

export default DetailHeader