import { Component } from 'react';
import {Link} from 'react-router-dom'
import blockedSVG from  '../assets/blocked.svg'
import './characterSelect.css'
import { updateCharacterStatus, chooseHoverImage, findCharacter} from '../store/index.js'

// character select element
// este elemento reaccióna  a la propiedad status de un personaje.
// los posibles status son: 'new", 'seen', o 'blocked'.
class CharacterSelect extends Component {
    constructor(props) {
        super(props)
        let { url, alt, id, status } = this.props
        this.state = {
            url,alt,id,status
        }
    }
    handleClick() {
        findCharacter(this.props.id)
    }

    render() {

        // se renderiza un elemento diferente para evitar renderizar 
        // mostrar información si el componente está bloqueado.
        switch (this.state.status) {
            case 'blocked':
                return this.templateBlocked()
            case 'new':
                return this.templateNew()
            default:
                return this.templateDefault()
                
        }
        
    }
    /* class methods */
    handleHover() {
        // al hacer hover, el articulo debe cambiar de estado.
        // para eso vamos a actualizar el registro  del elemento el store.
        // como doble seguro, este metodo solo se ejecuta si el template es new
        if (this.props.status === 'new') {
            updateCharacterStatus(this.props.id, 'seen')
            console.log('updated character')
        }
        if (this.props.status !== 'blocked') {
            console.log('update hover', this.props)
            chooseHoverImage(this.props)
        }
    }

    /* templates */
    templateDefault() {
        return (
            <div
                onMouseOver={() => { this.handleHover() }}
                onClick={()=>{this.handleClick()}}
                className={`column py-1 is-3  has-background-transparent
                ${componentClassName(this.props.status)}`}
            >
                <Link to={`/detail/${this.props.id}`} >
                    <figure className="image is-3by4 ">
                        <img
                            src={this.props.url}
                            alt={this.props.alt + this.props.id}/>
                    </figure>
                </Link>
            </div>
        )
    };
    
    templateNew() {
        /* 
        *  A diferencia del default, 
        *  este elemento tiene el 
        *  componente "Status Marker" 
        */
        return (
            <div
                onMouseOver={() => this.handleHover()}
                onClick={()=>{this.handleClick()}}
                className={`column is-3 py-1 has-background-transparent ${componentClassName(this.props.status)} is-relative`}>
                <Link to={`/detail/${this.props.id}`} >
                    <figure className="image is-3by4 has-filter  ">
                        <img src={this.props.url}
                            alt={this.props.alt + this.props.id} />
                    </figure>
                </Link>
                <StatusMarker/>
            </div>
        )
    }
    templateBlocked() {
        return (
            <div
                className={
                    `column
                    py-1
                    is-3
                   
                    is-relative
                    has-background-transparent
                    ${componentClassName(this.props.status)} 
                `}>
                <figure className="image is-3by4 has-background-grey-light">
                    <img src={blockedSVG} alt="" />
                </figure>
            </div>
        )
    }
}


// helper functions
function componentClassName(status) {
    switch (status) {
        case 'blocked':
            return 'is-blocked'
        case 'seen':
            return 'is-seen character-card'
        case 'new':
        default:
            return 'is-new character-card'
    }
}

/* 
TODO
function componentActive() {
    // función para mostrar posición del mouse
}
*/
// css that shows wheather the component is currently active or not
function StatusMarker() {
    let template = (
        <div className="tag is-absolute is-top  mx-1 my-1 ">New</div>
    )
    return template
    
}

export default CharacterSelect