
function Datum({ name, value, column }) {
    
    let columnSize,
        textSize,
        valueParse
    
    // asigna la clase correcta dependiendo del prop columna;
    switch (column) {
        case 'large':
            columnSize = 'column is-full'
            textSize = 'x6 font-regular'
            break
        case 'small':
        default:
            columnSize = 'column is-3-tablet is-half'
            textSize = 'x3 font-regular'
    }
    // parses value to avoid 'null' content
    switch (value) {
        case 'null':
            valueParse = 'TBD'
            break
        default:
            valueParse = value
    }
    
    return (
        <div className={`column ${columnSize}` }>
            
            <span className="name is-uppercase has-line-height-1 has-text-grey">{name}</span> <br/>
            <span className={`value ${textSize} has-line-height-1`}>{valueParse}</span>
        </div>        
    )
}
export default Datum