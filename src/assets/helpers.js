export function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

export function removeDuplicate(arr) {
    const filteredArr = arr.reduce((acc, current) => {
        const x = acc.find(item => item.id === current.id);
        if (!x) {
            return acc.concat([current]);
        } else {
            return acc;
        }
    }, []);
    return filteredArr;
}