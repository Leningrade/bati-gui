# BATI GUi
Proyecto de prueba para aplicar a PolymathV. Consume información de la 


## Documentos de diseño

UI: [x](https://www.figma.com/file/i8BiCOBe0v8LEVSFcKS2yo/Bati-GUi?node-id=0%3A1)
Mapa de Sitio: [x](https://www.figma.com/file/ChIkWnDU2LMUJzT5Skc5p4/Bati-GUI-Arbol-de-sitio?node-id=0%3A1)
planeación de servicio de renderizado: [x](https://www.figma.com/file/iSvIjt2ZmX1mF8Crlsslwv/BATI-GUI-Arq-de-llamadas?node-id=0%3A1)

## Historias de usuario
[X] Disponible para Mobil
[x] Solo son seleccionables los personajes dado el perfil (POC)
[] Renderiza, o están disponibles todos los personajes (ALT - 1)

## funciones
[x] Hover => Muestra imágen en grande
[x] manejo de estados  'blocked', 'seen', 'new'
[ ]
[x] Transición de carga
[x] Store para reducir las llamadas al server
## Responsables
-- Pending --


## Estados de Excepción manejados
[x] Página no existe
[x] El ID de heroe no está disponible para el usuario
    [] No estaba en la lista de numeros preseleccionados ( NO_ACCESS)
    [] No se encontró información del numero proporcionado (NO_INFO)
    [] Falló la conexión con el servidor (CONNECTION_ERROR)
[] No se encontró la imágen (convertir a blurb y precargar)

# Tool Docs

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `yarn build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
